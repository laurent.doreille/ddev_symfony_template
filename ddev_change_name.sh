#!/bin/bash

echo "Give the ddev project a new name"
read projectname
projectname="$( echo "$projectname" | sed 's/[[:space:]]/_/g')"
sed -i "1s/.*/name: ${projectname,,}/" test.yaml  
